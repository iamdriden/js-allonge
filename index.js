const unary = (fn) =>
  fn.lenth === 1
    ? fn
    : (something) => fn.call(this, something);


const tap = (val) =>
  (fn) => (typeof (fn) === "function" && fn(val), val);


const maybe = (fn) => (...args) => {
  if (args.length === 0) { return; }

  for (const arg of args) {
    if (arg == null) return;
  }

  return fn.apply(this, args);
}

const once = (fn) => {
  let done = false;

  return () => done ? void 0 : ((done = true), fn.apply(this, arguments));
}

const pipeline = (...fns) =>
  (value) =>
    fns.reduce((acc, fn) => fn(acc), value);

